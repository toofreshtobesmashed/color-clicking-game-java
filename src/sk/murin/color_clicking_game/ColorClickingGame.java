package sk.murin.color_clicking_game;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class ColorClickingGame implements KeyListener {

    private JFrame frame;
    private JButton buttonMiddle;
    private JButton[] buttons = new JButton[4];
    private Color[] colors = {Color.yellow, Color.red, Color.green, Color.blue};
    private Random r = new Random();
    private  Timer timer;
    private int sumYellow = 0;
    private int sumBlue = 0;
    private int sumGreen = 0;
    private int sumRed = 0;
    private int lastRandom = -1;

    public ColorClickingGame() {
        frame = new JFrame("Test your reflexes");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.addKeyListener(this);
        frame.setLayout(null);
        frame.setSize(700, 700);
        frame.setLocationRelativeTo(null);

        for (int i = 0; i < buttons.length; i++) {
            buttons[i] = new JButton();
            buttons[i].setEnabled(false);
            buttons[i].setFocusable(false);
            frame.add(buttons[i]);
        }

        buttons[0].setBounds(215, 30, 250, 82);
        buttons[1].setBounds(60, 200, 100, 250);
        buttons[2].setBounds(500, 200, 100, 250);
        buttons[3].setBounds(215, 500, 250, 82);

        buttonMiddle = new JButton("Play");
        buttonMiddle.setFont(new Font("Consolas", Font.BOLD, 26));
        buttonMiddle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start();
            }
        });
        buttonMiddle.setBounds(270, 250, 130, 130);
        buttonMiddle.setFocusable(false);
        frame.add(buttonMiddle);


        frame.setResizable(false);
        frame.setVisible(true);
    }

    private void start() {
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setEnabled(true);
            buttons[i].setFont(new Font("Consolas", Font.BOLD, 26));
            buttons[i].setBackground(colors[i]);
        }

        buttonMiddle.setText("");
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                showColor();
            }
        }, 0, 1000);
        buttonMiddle.setEnabled(false);
    }

    private void showColor() {
        int random = r.nextInt(colors.length);
        lastRandom = lastRandom == random ? (random + 1) % colors.length : random;
        buttonMiddle.setBackground(colors[lastRandom]);
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();

        switch (keyCode) {
            case KeyEvent.VK_UP:
                if (colors[lastRandom] == Color.yellow) {
                    sumYellow++;
                    buttons[0].setText(String.valueOf(sumYellow));
                    if (sumYellow > 5) {
                        finish();
                    }
                }
                System.out.println("hore " + sumYellow);
                break;
            case KeyEvent.VK_DOWN:
                if (colors[lastRandom] == Color.BLUE) {
                    sumBlue++;
                    buttons[3].setText(String.valueOf(sumBlue));
                    buttons[3].setForeground(Color.white);
                    if (sumBlue > 5) {
                        finish();
                    }
                }
                System.out.println("dole " + sumBlue);
                break;
            case KeyEvent.VK_LEFT:
                if (colors[lastRandom] == Color.RED) {
                    sumRed++;
                    buttons[1].setText(String.valueOf(sumRed));
                    if (sumRed > 5) {
                        finish();
                    }
                }
                System.out.println("vlavo " + sumRed);
                break;
            case KeyEvent.VK_RIGHT:
                if (colors[lastRandom] == Color.GREEN) {
                    sumGreen++;
                    buttons[2].setText(String.valueOf(sumGreen));
                    if (sumGreen > 5) {
                        finish();
                    }
                }
                System.out.println("vpravo " + sumGreen);
                break;
        }
    }

    private void finish() {
        JButton buttonFinish = new JButton();
        buttonMiddle.setBackground(buttonFinish.getBackground());
        buttonMiddle.setEnabled(false);
        timer.cancel();
        frame.removeKeyListener(this);
    }
}